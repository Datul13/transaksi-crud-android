package com.example.crud;

public class Config {

    public static final String URL_ADD="http://192.168.43.139/crud/tambah.php";
    public static final String URL_GET_ALL = "http://192.168.43.139/crud/tampilkan.php";
    public static final String URL_GET_VIEW = "http://192.168.43.139/crud/list.php?id=";
    public static final String URL_UPDATE = "http://192.168.43.139/crud/update.php";
    public static final String URL_DELETE = "http://192.168.43.139/crud/hapus.php?id=";

    //Dibawah ini merupakan Kunci yang akan digunakan untuk mengirim permintaan ke Skrip PHP
    public static final String KEY_ID = "id_transaksi";
    public static final String KEY_JENIS = "jenis";
    public static final String KEY_JML = "jumlah";
    public static final String KEY_TGL = "tanggal";
    public static final String KEY_KET = "keterangan";

    //JSON Tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id_transaksi";
    public static final String TAG_JENIS = "jenis";
    public static final String TAG_JML = "jumlah";
    public static final String TAG_TGL = "tanggal";
    public static final String TAG_KET = "keterangan";

    public static final String TRANS_ID = "id_transaksi";

}
