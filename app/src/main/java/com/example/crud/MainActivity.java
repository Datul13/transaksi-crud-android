package com.example.crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextTgl;
    private EditText editTextKet;
    private EditText editTextJml;
    private Spinner spinnerJenis;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Button btDatePicker;
    private EditText tvDateResult;

    private Button buttonAdd;
    private Button buttonView;

    private static final String[] PIL = {"Pemasukan", "Pengeluaran"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        editTextTgl = (EditText) findViewById(R.id.editTextTgl);
        editTextKet = (EditText) findViewById(R.id.editTextKeterangan);
        editTextJml = (EditText) findViewById(R.id.editTextJumlah);
        spinnerJenis = (Spinner) findViewById(R.id.spinnerCategory);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, PIL);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerJenis.setAdapter(adapter);


        buttonAdd = (Button) findViewById(R.id.btnsave);
        buttonView = (Button) findViewById(R.id.btnList);

        buttonAdd.setOnClickListener(this);
        buttonView.setOnClickListener(this);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        tvDateResult = (EditText) findViewById(R.id.date);
        btDatePicker = (Button) findViewById(R.id.calendar);
        btDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

    }

    private void showDateDialog() {

        /**
         * Calendar untuk mendapatkan tanggal sekarang
         */
        Calendar newCalendar = Calendar.getInstance();

        /**
         * Initiate DatePicker dialog
         */
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                /**
                 * Method ini dipanggil saat kita selesai memilih tanggal di DatePicker
                 */

                /**
                 * Set Calendar untuk menampung tanggal yang dipilih
                 */
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tvDateResult.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        datePickerDialog.show();
    }

    private void add(){

        final String tgl = editTextTgl.getText().toString().trim();
        final String jenis = spinnerJenis.getContext().toString().trim();
        final String jml = editTextJml.getText().toString().trim();
        final String ket = editTextKet.getText().toString().trim();

        class Add extends AsyncTask<Void,Void,String>{

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(MainActivity.this,"Menambahkan...","Tunggu...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(MainActivity.this,s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... v) {
                HashMap<String,String> params = new HashMap<>();
                params.put(Config.KEY_TGL,tgl);
                params.put(Config.KEY_JENIS,jenis);
                params.put(Config.KEY_JML,jml);
                params.put(Config.KEY_KET,ket);

                Handler rh = new Handler();
                String res = rh.sendPostRequest(Config.URL_ADD, params);
                return res;
            }
        }

        Add ae = new Add();
        ae.execute();
    }

    @Override
    public void onClick(View v) {
        if(v == buttonAdd){
            add();
        }

        if(v == buttonView){
            startActivity(new Intent(this,listTransaksi.class));
        }
    }

}