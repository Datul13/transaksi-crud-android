package com.example.crud;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LihatTransaksi extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextId;
    private EditText editTextTgl;
    private EditText editTextKet;
    private EditText editTextJml;
    private EditText editTextJenis;

    private Button buttonUpdate;
    private Button buttonDelete;

    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_lihat_transaksi );

        Intent intent = getIntent();

        id = intent.getStringExtra(Config.TRANS_ID);

        editTextId = (EditText) findViewById(R.id.editTextId);
        editTextTgl = (EditText) findViewById(R.id.editTextTgl);
        editTextKet = (EditText) findViewById(R.id.editTextKeterangan);
        editTextJml = (EditText) findViewById(R.id.editTextJumlah);
        editTextJenis = (EditText) findViewById(R.id.editTextJenis);

        buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
        buttonDelete = (Button) findViewById(R.id.buttonDelete);

        buttonUpdate.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);

        editTextId.setText(id);

        getEmployee();
    }

    private void getEmployee(){
        class GetEmployee extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LihatTransaksi.this,"Fetching...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                showEmployee(s);
            }

            @Override
            protected String doInBackground(Void... params) {
                Handler rh = new Handler();
                String s = rh.sendGetRequestParam(Config.URL_GET_VIEW,id);
                return s;
            }
        }
        GetEmployee ge = new GetEmployee();
        ge.execute();
    }

    private void showEmployee(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray result = jsonObject.getJSONArray(Config.TAG_JSON_ARRAY);
            JSONObject c = result.getJSONObject(0);
            String tgl = c.getString(Config.TAG_TGL);
            String jenis = c.getString(Config.TAG_JENIS);
            String jml = c.getString(Config.TAG_JML);
            String ket = c.getString(Config.TAG_KET);

            editTextTgl.setText(tgl);
            editTextKet.setText(ket);
            editTextJml.setText(jml);
            editTextJenis.setText(jenis);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void update(){
        final String tgl = editTextTgl.getText().toString().trim();
        final String jenis = editTextJenis.getText().toString().trim();
        final String jml = editTextJml.getText().toString().trim();
        final String ket = editTextKet.getText().toString().trim();

        class Update extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LihatTransaksi.this,"Updating...","Wait...",false,false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(LihatTransaksi.this,s,Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put(Config.KEY_ID,id);
                hashMap.put(Config.KEY_TGL,tgl);
                hashMap.put(Config.KEY_JENIS,jenis);
                hashMap.put(Config.KEY_JML,jml);
                hashMap.put(Config.KEY_KET,ket);

                Handler rh = new Handler();

                String s = rh.sendPostRequest(Config.URL_UPDATE,hashMap);

                return s;
            }
        }

        Update ue = new Update();
        ue.execute();
    }

    private void delete(){
        class Delete extends AsyncTask<Void,Void,String> {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(LihatTransaksi.this, "Updating...", "Tunggu...", false, false);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                loading.dismiss();
                Toast.makeText(LihatTransaksi.this, s, Toast.LENGTH_LONG).show();
            }

            @Override
            protected String doInBackground(Void... params) {
                Handler rh = new Handler();
                String s = rh.sendGetRequestParam(Config.URL_DELETE, id);
                return s;
            }
        }

        Delete de = new Delete();
        de.execute();
    }

    private void confirmDelete(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah Kamu Yakin Ingin Menghapus Pegawai ini?");

        alertDialogBuilder.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        delete();
                        startActivity(new Intent(LihatTransaksi.this,listTransaksi.class));
                    }
                });

        alertDialogBuilder.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        if(v == buttonUpdate){
            update();
        }

        if(v == buttonDelete){
            confirmDelete();
        }
    }

}