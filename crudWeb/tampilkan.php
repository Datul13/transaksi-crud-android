<?php

    //Mendapatkan Nilai Dari Variable ID Pegawai yang ingin ditampilkan
    $id = $_POST['id_transaksi'];

    //Importing database
    require_once('koneksi.php');

    //Membuat SQL Query dengan pegawai yang ditentukan secara spesifik sesuai ID
    $sql = "SELECT * FROM transaksi WHERE id_transaksi=$id";

    //Mendapatkan Hasil
    $r = mysqli_query($con,$sql);

    //Memasukkan Hasil Kedalam Array
    $result = array();
    $row = mysqli_fetch_array($r);
    array_push($result,array(
            "kategori"=>$row['kategori'],
            "jumlah"=>$row['jumlah'],
            "tanggal"=>$row['tanggal'],
            "keterangan"=>$row['keterangan']
        ));

    //Menampilkan dalam format JSON
    echo json_encode(array('result'=>$result));

    mysqli_close($con);

?>